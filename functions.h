#ifndef CGI_FUNCTIONS_H
#define CGI_FUNCTIONS_H

#endif //CGI_FUNCTIONS_H

#include <string.h>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sstream>
#include <iostream>
#include <sys/fcntl.h>
#include <sstream>
#include <fstream>
#include <unistd.h>
#include <cstdlib>

void package(std::string path, int client_socket);
std::string parser_url(char *get);
void write_headers_of_error (int client_socket, int status, size_t content_length, const char *data);
std::string parser_method(char* buf);
std::string type_handler(std::string path);
