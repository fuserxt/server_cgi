#include <string.h>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sstream>
#include <iostream>
#include <sys/fcntl.h>
#include <sstream>
#include <fstream>
#include <unistd.h>
#include <cstdlib>

#include "functions.h"

void write_headers_of_error (int client_socket, int status, size_t content_length, const char *data) {
    char headers[2048];
    if (200 != status)
    {
        sprintf(headers, "HTTP/1.0 %d %s\r\n"
                        "Server: cs_cgi\r\n"
                        "Content-Type: text/plain\r\n"
                        "Content-Length: %d\r\n"
                        "\r\n"
                        "%s",
                status, data, strlen(data), data);
    }
    write(client_socket, headers, strlen(headers));
}
