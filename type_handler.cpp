#include <string.h>

#include "functions.h"

std::string type_handler(std::string path) 
{
    if (path.find(".html") != std::string::npos) 
    {
        return "text/html";
    }
    else if (path.find(".jpg") != std::string::npos) 
    {
        return "image/jpg";
    }
    else if (path.find(".jpeg") != std::string::npos) 
    {
        return "image/jpeg";
    }
    else if (path.find(".png") != std::string::npos)
    {
	return "image/png";
    }
    return "text/plain; charset=utf-8";
}
