#include <string.h>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sstream>
#include <iostream>
#include <sys/fcntl.h>
#include <sstream>
#include <fstream>
#include <unistd.h>
#include <cstdlib>
#include <sys/wait.h>
#include <stdio.h>
#include <ctype.h>
#include <locale>
#include <sstream>
#include <iterator>
#include <algorithm>

#include "functions.h"

bool port_exists(int dwPort) {  
	struct sockaddr_in addr;         

	int sock = socket(AF_INET, SOCK_STREAM, 0);
        addr.sin_family = AF_INET;
        addr.sin_port = dwPort;
        addr.sin_addr.s_addr = htonl(INADDR_ANY);
	int result = bind(sock, (struct sockaddr *) &addr, sizeof(addr));
	close(sock);
    return result >= 0 ? true : false; 
}  

int main(int argc, char **argv) {
    int port = 0;
    int port_1 = 0;
    std::string p = "";
    char buf[255];

    int client_socket;
    int sock;
    struct sockaddr_in addr;
    size_t bytes_received;
    if (argc > 3 || argc == 1) {
	perror("error argument");
        exit(2);
    } else if (argc == 2) {
	strcpy(buf, argv[1]);
	if (isdigit(*argv[1])) {
	    port = atoi(argv[1]);
            port_1 = htons(port);
	    char dir[16600];
	    p = getcwd(dir, 16600);

            sock = socket(AF_INET, SOCK_STREAM, 0);
            addr.sin_family = AF_INET;
            addr.sin_port = port_1;
            addr.sin_addr.s_addr = htonl(INADDR_ANY);

	    std::cout << p << std::endl;
	}
        else if (argv[1][0] == '/') {
		p = argv[1];
                sock = socket(AF_INET, SOCK_STREAM, 0);
		int port = 1024;
		while (port < 8001) {
			if (port_exists(port)) {
				break;
			}
			++port;
		}
		if (port == 8001) {
			std::cout << "Error" << std::endl;
		} else {
			std::cout << "port: " << port << std::endl;
			addr.sin_family = AF_INET;
			addr.sin_port = port;
			addr.sin_addr.s_addr = htonl(INADDR_ANY);
		}
	}
    } else if (argc == 3) {
	if (!isdigit(*argv[1]))
	{
	    perror("error argument - port");
	    exit(2);
	} 
	port = atoi(argv[1]);
        port_1 = htons(port);
	if (argv[2][0] != '/')
	{
	    perror("error argument - way");
	    exit(2);
	}
        p = argv[2];

	sock = socket(AF_INET, SOCK_STREAM, 0);
        addr.sin_family = AF_INET;
        addr.sin_port = port_1;
        addr.sin_addr.s_addr = htonl(INADDR_ANY);
    }
    std::string st = p + "/";

    if (bind(sock, (struct sockaddr *) &addr, sizeof(addr)) < 0)
    {
        perror("bind");
        exit(2);
    }


    listen(sock, 5);
    while (true)
    {
        client_socket = accept(sock, NULL, NULL); // для общения с клентом создает новый сокет и возвращает его дескриптор
	std::cout << "client socket: " << client_socket << std::endl;        
	if (client_socket > 0)
        {
            int pid;
            if (!(pid=fork()))
            {
                char recv_bufer[16600];
                std::string str;
                bytes_received = read(client_socket, recv_bufer, sizeof(recv_bufer)); // возвращает число байт, что было отправлено

                //std::string method = parser_method(recv_bufer);
		
                str = parser_url(recv_bufer);
                std::string url = (st + str);
                if (bytes_received == 0)
                {
                    std::cerr << "close connection\n";
                }
                else if (bytes_received == -1)
                {
                    std::cerr << "recv: " << bytes_received << "\n";
                    close(client_socket);
                }
                else
                {
                    package(url, client_socket);

                    close(client_socket);
                    exit(0);
                }
            }
            else
            {
                wait(NULL);
                close(client_socket); // те родительский закрывает сокет
            }
        }
        else
        {
            perror("accept");
            exit(3);
        }
    }
    return 0;
}
