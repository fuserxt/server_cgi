#include <string.h>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sstream>
#include <iostream>
#include <sys/fcntl.h>
#include <sstream>
#include <fstream>
#include <unistd.h>
#include <cstdlib>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "functions.h"

void package(std::string path, int client_socket)
{	
    int fd;
    fd = open(path.c_str(), O_RDONLY, 0666);

    if (fd != -1)
    {
	std::stringstream response_1;
	response_1 << "HTTP/1.1 200 OK\r\n"
        << "Version: HTTP/1.1\r\n"
        << "Content-Type: " << type_handler(path) << "\r\n"
        << "Content-Length: ";
        write(client_socket, response_1.str().c_str(), response_1.str().length());

	struct stat bf;
	lstat(path.c_str(), &bf);
	size_t size = bf.st_size;
	std::stringstream response_2;
	response_2 << size << "\r\n\r\n";
	write(client_socket, response_2.str().c_str(), response_2.str().length());

	int bytes_read = 0;
	char buff[1024];

        while ((bytes_read = read(fd, buff, sizeof(buff))) > 0) {
            write(client_socket, buff, bytes_read);
        }
    }
    else
    {
        write_headers_of_error(client_socket, 404, 0, "NOT FOUND");
        perror("open");
        exit(1);
    }
}
